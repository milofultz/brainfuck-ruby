filepath = ARGV[0]

unless filepath.is_a?(String)
  puts "Requires a file as argument"
  exit
end

DATA_LENGTH = 1000

instructions = File.open(filepath).read()

data = [0] * DATA_LENGTH
p_data = 0
p_instructions = 0

while (char = instructions[p_instructions]) != nil
  case char
    when "+"
      if data[p_data] === 255
        data[p_data] = 0
      else
        data[p_data] += 1
      end
    when "-"
      if data[p_data] === 0
        data[p_data] = 255
      else
        data[p_data] -= 1
      end
    when ">"
      p_data += 1
    when "<"
      p_data -= 1
    when "."
      char_in_data = data[p_data].chr
      print(char_in_data)
    when ","
      user_input = $stdin.gets.chomp()
      user_input = user_input[0] || "\n"
      data[p_data] = user_input.ord
    when "["
      if data[p_data] == 0
        # This is so the first bracket is added to make it 0
        bracket_count = -1
        while true
          case instructions[p_instructions]
            when "]"
              if bracket_count == 0
                break
              end
              bracket_count -= 1
            when "["
              bracket_count += 1
          end
          p_instructions += 1
        end
      end
    when "]"
      if data[p_data] != 0
        # setting to 1 so that first bracket_count decrements
        bracket_count = 1
        while true
          case instructions[p_instructions]
            when "["
              if bracket_count == 0
                break
              else
                bracket_count += 1
              end
            when "]"
              bracket_count -= 1
          end
          p_instructions -= 1
        end
      end
  end
  p_instructions += 1
end

